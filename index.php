<?php

// 学习笔记
// https://learnku.com/articles/6652/learn-to-develop-their-own-composer-package-and-to-use-packagist-github-updates 
require_once './vendor/autoload.php'; // 加载自动加载文件

use Flower\Rose\Rose;
use Flower\Lily\Lily;
// require_once './src/Tarfly.php'; 


$rose = new Rose();
echo $rose->desc();
echo PHP_EOL;


$lily = new Lily();
echo $lily->desc();
echo PHP_EOL;

use Tarfly\Tarfly;
$tarfly = new Tarfly();
echo $tarfly->desc();
echo PHP_EOL;


use Xwz\Wzb;

$wzb = new Wzb();

echo $wzb->desc();

echo PHP_EOL;



use Xwz\Xwz\Xwz;
$xwz = new Xwz();
echo $xwz->desc();
echo PHP_EOL;
exit;


